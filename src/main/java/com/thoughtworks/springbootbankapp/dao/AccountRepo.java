package com.thoughtworks.springbootbankapp.dao;

import com.thoughtworks.springbootbankapp.entity.bankAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepo extends CrudRepository<bankAccount, Integer> {

}
