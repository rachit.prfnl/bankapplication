package com.thoughtworks.springbootbankapp.controllers;

import com.thoughtworks.springbootbankapp.dao.AccountRepo;
import com.thoughtworks.springbootbankapp.entity.bankAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {
    @Autowired
    private AccountRepo repo;

    @GetMapping
    public Iterable<bankAccount> getAllCustomers(){
        return repo.findAll();
    }
}
