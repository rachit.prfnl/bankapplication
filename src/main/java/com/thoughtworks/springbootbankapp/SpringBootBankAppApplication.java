package com.thoughtworks.springbootbankapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SpringBootBankAppApplication {

	@GetMapping
	public String sayHello(){
		return "Hello from Rachit !";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBankAppApplication.class, args);
	}

}
